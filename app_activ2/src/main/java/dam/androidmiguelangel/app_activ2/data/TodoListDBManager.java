package dam.androidmiguelangel.app_activ2.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import dam.androidmiguelangel.app_activ2.model.Task;

public class TodoListDBManager {

    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager(Context context){
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    public void insert(String todo, String when, String description, String priority, String status){

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null){
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Tasks.TODO, todo);
            contentValue.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValue.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValue.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValue.put(TodoListDBContract.Tasks.STATUS, status);

            sqLiteDatabase.insert(TodoListDBContract.Tasks.TABLE_NAME, null, contentValue);
        }

    }

    public void update(String id, String todo, String when, String description, String priority, String status){

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null){
            ContentValues contentValues = new ContentValues();

            contentValues.put(TodoListDBContract.Tasks.TODO, todo);
            contentValues.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValues.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValues.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValues.put(TodoListDBContract.Tasks.STATUS, status);

            String condition = TodoListDBContract.Tasks._ID + "=" + id;

            sqLiteDatabase.update(TodoListDBContract.Tasks.TABLE_NAME, contentValues, condition, null);
        }

    }

    public void delete(String id){

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null){

            String condition = TodoListDBContract.Tasks._ID + "=" + id;

            sqLiteDatabase.delete(TodoListDBContract.Tasks.TABLE_NAME, condition, null);

        }

    }

    public ArrayList<Task> getTasks(){
        ArrayList<Task> taskList = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null){
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
            TodoListDBContract.Tasks.TODO,
            TodoListDBContract.Tasks.TO_ACCOMPLISH,
            TodoListDBContract.Tasks.DESCRIPTION,
            TodoListDBContract.Tasks.PRIORITY,
            TodoListDBContract.Tasks.STATUS};

            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null);

            if (cursorTodoList != null){
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                while (cursorTodoList.moveToNext()){
                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);
                }

                cursorTodoList.close();
            }

        }

        return taskList;

    }

    public void close() {
        todoListDBHelper.close();
    }

}
