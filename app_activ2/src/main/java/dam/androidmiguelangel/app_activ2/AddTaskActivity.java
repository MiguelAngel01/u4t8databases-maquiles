package dam.androidmiguelangel.app_activ2;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import dam.androidmiguelangel.app_activ2.data.TodoListDBManager;

public class AddTaskActivity extends AppCompatActivity {

    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spPriority;
    private Spinner spStatus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        setUI();

    }

    private void setUI(){

        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);
        spPriority = findViewById(R.id.spPriority);
        spStatus = findViewById(R.id.spStatus);

        ArrayAdapter<CharSequence> spPriorityAdapter = ArrayAdapter.createFromResource(this, R.array.priority, android.R.layout.simple_spinner_item);
        spPriorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(spPriorityAdapter);

        ArrayAdapter<CharSequence> spStatusAdapter = ArrayAdapter.createFromResource(this, R.array.status, android.R.layout.simple_spinner_item);
        spStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(spStatusAdapter);

    }

    public void onClick(View view) {

        if (view.getId() == R.id.buttonOK){
            if (etTodo.getText().toString().length() > 0){
                TodoListDBManager todoListDBManager = new TodoListDBManager(this);

                todoListDBManager.insert(etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spPriority.getSelectedItem().toString(),
                        spStatus.getSelectedItem().toString());
            } else {
                Toast.makeText(this, "\"Task name is empty\"", Toast.LENGTH_LONG).show();
            }

            finish();

        }

    }
}
