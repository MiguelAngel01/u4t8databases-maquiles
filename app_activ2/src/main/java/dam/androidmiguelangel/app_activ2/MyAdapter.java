package dam.androidmiguelangel.app_activ2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidmiguelangel.app_activ2.data.TodoListDBManager;
import dam.androidmiguelangel.app_activ2.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;
    public static Context context;

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvPriorityValue;
        TextView tvStatus;

        public MyViewHolder(View view){
            super(view);

            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvPriorityValue = view.findViewById(R.id.tvPriorityValue);
            this.tvStatus = view.findViewById(R.id.tvStatus);
        }

        public void bind(Task task){

            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescprition());
            this.tvPriorityValue.setText(task.getPriority());
            this.tvStatus.setText(task.getStatus());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EditTaskActivity.class);
                    intent.putExtra("id", tvId.getText().toString());
                    intent.putExtra("todo", tvTodo.getText().toString());
                    intent.putExtra("toAccomplish", tvToAccomplish.getText().toString());
                    intent.putExtra("description", tvDescription.getText().toString());
                    intent.putExtra("priority", tvPriorityValue.getText().toString());
                    intent.putExtra("status", tvStatus.getText().toString());
                    context.startActivity(intent);
                }
            });

        }

    }

    public MyAdapter(Context context, TodoListDBManager todoListDBManager){
        this.context = context;
        this.todoListDBManager = todoListDBManager;
    }

    public void getData(){
        this.myTaskList = todoListDBManager.getTasks();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);

        return new MyViewHolder(itemLayout);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.bind(myTaskList.get(position));

    }

    @Override
    public int getItemCount() {
        return myTaskList.size();
    }
}
