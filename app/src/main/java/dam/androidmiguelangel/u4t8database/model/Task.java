package dam.androidmiguelangel.u4t8database.model;

public class Task {

    private int _id;
    private String todo;
    private String toAccomplish;
    private String descprition;

    public Task(int _id, String todo, String toAccomplish, String descprition) {
        this._id = _id;
        this.todo = todo;
        this.toAccomplish = toAccomplish;
        this.descprition = descprition;
    }

    public int get_id() {
        return _id;
    }

    public String getTodo() {
        return todo;
    }

    public String getToAccomplish() {
        return toAccomplish;
    }

    public String getDescprition() {
        return descprition;
    }
}
