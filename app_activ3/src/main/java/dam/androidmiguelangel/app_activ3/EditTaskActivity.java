package dam.androidmiguelangel.app_activ3;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import dam.androidmiguelangel.app_activ3.data.TodoListDBManager;

public class EditTaskActivity extends AppCompatActivity {

    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spPriority;
    private Spinner spStatus;
    private Button btSave;
    private Button btDelete;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        id = getIntent().getExtras().getString("id");

        setUI();

    }

    private void setUI(){

        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);
        spPriority = findViewById(R.id.spPriority);
        spStatus = findViewById(R.id.spStatus);
        btSave = findViewById(R.id.btSave);
        btDelete = findViewById(R.id.btDelete);

        etTodo.setText(getIntent().getExtras().getString("todo"));
        etToAccomplish.setText(getIntent().getExtras().getString("toAccomplish"));
        etDescription.setText(getIntent().getExtras().getString("description"));

        ArrayAdapter<CharSequence> spPriorityAdapter = ArrayAdapter.createFromResource(this, R.array.priority, android.R.layout.simple_spinner_item);
        spPriorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(spPriorityAdapter);

        ArrayAdapter<CharSequence> spStatusAdapter = ArrayAdapter.createFromResource(this, R.array.status, android.R.layout.simple_spinner_item);
        spStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(spStatusAdapter);

        for (int i = 0; i < spPriority.getCount(); i++){

            if (spPriority.getItemAtPosition(i).toString().equalsIgnoreCase(getIntent().getExtras().getString("priority"))){
                spPriority.setSelection(i);
            }

        }

        for (int i = 0; i < spStatus.getCount(); i++){

            if (spStatus.getItemAtPosition(i).toString().equalsIgnoreCase(getIntent().getExtras().getString("status"))){
                spStatus.setSelection(i);
            }

        }

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TodoListDBManager todoListDBManager = new TodoListDBManager(getApplicationContext());

                todoListDBManager.update(id,
                        etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spPriority.getSelectedItem().toString(),
                        spStatus.getSelectedItem().toString());

                finish();

            }
        });

        btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TodoListDBManager todoListDBManager = new TodoListDBManager(getApplicationContext());

                todoListDBManager.delete(id);

                finish();
            }
        });

    }

}