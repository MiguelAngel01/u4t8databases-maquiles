package dam.androidmiguelangel.app_activ3.model;

public class Task {

    private int _id;
    private String todo;
    private String toAccomplish;
    private String descprition;
    private String priority;
    private String status;

    public Task(int _id, String todo, String toAccomplish, String descprition, String priority, String status) {
        this._id = _id;
        this.todo = todo;
        this.toAccomplish = toAccomplish;
        this.descprition = descprition;
        this.priority = priority;
        this.status = status;
    }

    public int get_id() {
        return _id;
    }

    public String getTodo() {
        return todo;
    }

    public String getToAccomplish() {
        return toAccomplish;
    }

    public String getDescprition() {
        return descprition;
    }

    public String getPriority() {
        return priority;
    }

    public String getStatus() {
        return status;
    }
}
