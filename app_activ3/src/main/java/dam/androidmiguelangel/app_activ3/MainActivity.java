package dam.androidmiguelangel.app_activ3;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import dam.androidmiguelangel.app_activ3.data.TodoListDBManager;
import dam.androidmiguelangel.app_activ3.model.Task;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todoListDBManager = new TodoListDBManager(this);
        myAdapter = new MyAdapter(this, todoListDBManager);

        setUI();

    }

    private void setUI(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((view) -> {
            startActivity(new Intent(getApplicationContext(), AddTaskActivity.class));
        });

        rvTodoList = findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvTodoList.setAdapter(myAdapter);

    }

    @Override
    protected void onResume() {

        super.onResume();

        myAdapter.getData();

    }

    @Override
    protected void onDestroy() {

        todoListDBManager.close();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        TodoListDBManager todoListDBManager = new TodoListDBManager(getApplicationContext());

        ArrayList<Task> tasks;

        switch (item.getItemId()){
            case R.id.not_started:
                tasks = todoListDBManager.getTasksByStatus("Not Started");
                myAdapter.setData(tasks);
                break;
            case R.id.on_progress:
                tasks = todoListDBManager.getTasksByStatus("In Progress");
                myAdapter.setData(tasks);
                break;
            case R.id.completed:
                tasks = todoListDBManager.getTasksByStatus("Completed");
                myAdapter.setData(tasks);
                break;
            case R.id.all:
                tasks = todoListDBManager.getTasks();
                myAdapter.setData(tasks);
                break;
            case R.id.delete_completed:
                todoListDBManager.deleteCompleted("Completed");
                myAdapter.getData();
                break;
            case R.id.delete_all:
                todoListDBManager.deleteAll();
                myAdapter.getData();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}